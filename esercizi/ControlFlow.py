''' Control Flow '''
##Libreria per restituire eventuali messaggi di errore, interrompendo l'esecuzione dello script
import sys
##Libreria per poter sviluppare valori randomici
import random
##Libreria per il calcolo della radice quadrata di un oggetto
import numpy as np

##Funzione per l'input utente che rispetti i parametri previsi
def check_x(prompt):
    while True:
        try:
            x = int(input(prompt))
            
            if x<1:
                print('\nIl valore inserito non può essere utilizzato, deve essere maggiore di 0')
            else:
                print('\nIl valore inserito è utilizzabile per le prossime operazioni!')
                break
                
        except ValueError:
            print('\nIl valore inserito non è un numero intero')
              
    return x
     
     
x=check_x('Inserisci un valore: ')
        
###Creiamo le liste######

a_list = []
b_list = []

##Sviluppiamo le liste randomicamente
for i in range(x):
    r = random.randint(0, 101)
    if len(a_list) == 0:
        a_list = [r]
    else:
        a_list.append(r)

for i in range(x):
    r = random.randint(0, 101)
    if len(a_list) == 0:
        b_list = [r]
    else:
        b_list.append(r)

##Stampa delle liste e verifica che siano venute in modo corretto    
print('\nLa prima lista è la seguente:', a_list)
print('La seconda lista è la seguente:', b_list)
if x == len(a_list) and x==len(b_list):
    print('\nSi sono create due liste con elementi randomici delle dimensioni del valore inizialmente inserito')
else:
    print('\nNon si sono realizzate le liste correttamente')
    
##List comprehension. Creiamo le possibili combinazioni di 2 elementi fra le 2 liste se i valori sono diversi, lo facciamo per numeri piccoli o verrebbe una cosa che riempie il terminale
if x<4:
    '''
    c_list = []
    for i in a_list:
        for j in b_list:
            if i != j:
                c_list.append((i,j))
    '''
    c_list = [(i, j) for i in a_list for j in b_list if i != j]
    print('La lista ottenuta combinando le prime 2 di liste escludendone duplicati:', c_list)

###Creiamo una tupla dalla lista sviluppata###
a_tuple = tuple(a_list)

print('\nLa tupla è la seguente:', a_tuple)

###Creiamo i set dalle liste sviluppate###
a_set = set(a_list)
b_set = set(b_list)

print('\nIl primo set è il seguente: ', a_set)
print('Il secondo set è il seguente: ', b_set)

##Creiamo un terzo set dall'unione dei primi due
c_set = a_set.union(b_set)
c = len(list(c_set))
sumValue = 2*x

print('\nUniamo i due set per avere un unico set di', c, 'elementi', c_set)
if c == sumValue:
    print('\nI due set non avevano elementi in comune pertanto il terzo set ha la stessa dimensione della somma dei primi due')
else:
    print('\nI due set avevano elementi in comune perchè la somma fra i due è di', sumValue, 'elementi mentre il terzo set risultante dalla unione ha', c, 'elementi')

##Set comprehension per sviluppare i quadrati dei numeri da 0 a 10 e poi verifica se sono presenti nel set precedentemente unito
square_numbers_set = {n**2 for n in range(11)}
 
print('\nNumeri al quadrato:', square_numbers_set)
intersection = c_set.intersection(square_numbers_set)
void_set = set()

if intersection != void_set:
    print('\nIntersezioni fra il set unito e quello dei numeri al quadrato:', intersection)
    #basic_set = intersection ** (0.5) attuabile se intersection non fosse un oggetto contenente più valori
    basic_set = np.sqrt(list(intersection))

    if basic_set.size == 0:
        print('\nNel set risultante dalla unione non è presente nessun quadrato e finire qua sarebbe grave perchè non è possibile')
    else:
        print('\nGli elementi al quadrato contenuti nel set unito precedentemente hanno queste radici:', basic_set) 
else:
    print('\nNessun elemento comune fra il set unito precedentemente e il set dei quadrati dei valori da 0 a 10, pertanto non si ha nessuna intersezione')
    

###Stringhe###
s0 = 'Questa è una storia di tanto tempo fa, '
s1 = 'quando vostro nonno era ancora bambino, '
s2 = 'ed è molto importante perchè fa vedere come siano cominciati i va e vieni dalla terra di Narnia, '
s3 = 'In quei tempi Sherlock holmes abitava ancora in Baker Street e i sei ragazzi Bastable cercavano tesori in piena Londra, '
s4 = 'sulla Lewisham Road, '

s = s4 +s2 + s1 + s0 + s3

#print('\n', s)
print('\n', s)
s1_list = s.split(',')
print('\n', s1_list)
reorder_list = []
#extend crea una sottolista di più elementi all'interno della lista
reorder_list.extend([s1_list[3], s1_list[2],s1_list[1],s1_list[4],s1_list[0]])
print('\n', reorder_list)