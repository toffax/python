// Le classi rispettano anche loro le proprietà di indentazione, pass è uno statement solo per riempire la classe che altrimenti non avrebbe senso e modo di esistere
// pass è un set vuoto, in Java o C corrisponderebbe a ({})
class PapayaWhip:  
    pass

// Il primo argomento di ogni classe è sempre riferito all’istanza corrente ed in questo caso dal self
class Fib:
    '''iterator that yields numbers in the Fibonacci sequence'''  

    def __init__(self, max):   
// In generale self fa riferimento sempre all’istanza dove è stato chiamato ma nel caso sopra funge anche da metodo di creazione per un nuovo oggetto
// self.max è una variabile in quella istanza non si riferisce ad altre variabili, essendo globale nell'istanza può essere acceduta da altri metodi solamente propri dell'istanza. In questo caso della classe
class Fib:
    def __init__(self, max):
        self.max = max
// I metodi aventi __metodi__ vengono definiti metodi speciali, perchè non sono chiamati direttamente ma solamente dalla classe 
// Una classe iteratrice è una classe avente il metodo iter(), per averlo non si può sviluppare in una funzione ma è necessaria una classe
// Il "raise" StopIteration è un'eccezione che non restituisce un errore ma avverte il generatore che non è più necessario sviluppare altri valori, può anche essere utilizzato nei loop per uscire dai for
 def __next__(self):                           
        fib = self.a
        if fib > self.max:
            raise StopIteration                   
        self.a, self.b = self.b, self.a + self.b
        return fib      