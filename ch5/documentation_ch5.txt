// Regular expressions
// leggere il modulo re per avere un anteprima di quanto segue: https://docs.python.org/3/library/re.html
>>> import re
// Questo modulo permette di creare delle operazioni indipendenti
// Fino ad ora si sono visti medoti dipendenti e potrebbero non essere opportuni nell'eventualità ci siano leggere differenze
// Per sostituire un termine alla fine della stringa(s) si pone il $ e si utilizza la funzione re.sub() per ricercare
>>> re.sub('da_sostituire$', 'sostituto', s)
// Per sostituire un termine all'inizio della stringa(s) si pone ^
// Per le regular expressions è consigliato usare sempre la notazione “raw string” ponendo la r nel primo argomento
>>> re.sub(r’\bROAD\b’, ‘RD.’, s)
// Da visionare https://docs.python.org/3/library/re.html per tutte le possibili sintassi, che sono una marea
// ? che segue un carattere indica che può essere opzionale, | indica un pattern da scegliere fra più opzioni, () includono i pattern di cui uno è da scegliere
// (A|B|C) significa matcha esattamente con uno fra A, B o C
>>> pattern = '^M?M?M?(CM|CD|D?C?C?C?)(XC|XL|L?X?X?X?)$'

// Sintassi {n,m}, nel caso seguente questo pattern serve per matchare da un minimo di 0 fino ad un massimo di 3 caratteri M, di cui uno deve essere all’inizio ed uno alla fine
>>> pattern = ‘^M{0,3}$’
>>> pattern = '^M{0,3}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$'
Il seguente comando non restituirà None perchè con il pattern qui sopra è possibile ricadere sempre nella scelta di valori opzionali
>>> re.search(pattern, 'I')

// Per poter utilizzare le regular expression e comprenderle anche dopo diversi periodi si utilizzano le verbose regular expressions
// Esse sono identiche alle regular expression se non che è possibile commentare ogni riga, e per ogni riga è presente un comando della regular expressions
// Le verbose infatti ignorano i whitespace(spazi, tabs) ed i commenti
>>> pattern = '''
    ^                   # beginning of string
    M{0,3}              # thousands - 0 to 3 Ms
    (CM|CD|D?C{0,3})    # hundreds - 900 (CM), 400 (CD), 0-300 (0 to 3 Cs),
                        #            or 500-800 (D, followed by 0 to 3 Cs)
    (XC|XL|L?X{0,3})    # tens - 90 (XC), 40 (XL), 0-30 (0 to 3 Xs),
                        #        or 50-80 (L, followed by 0 to 3 Xs)
    (IX|IV|V?I{0,3})    # ones - 9 (IX), 4 (IV), 0-3 (0 to 3 Is),
                        #        or 5-8 (V, followed by 0 to 3 Is)
    $                   # end of string
    '''
Per utilizzare le verbose regular expressions è necessario passare un extra argomento, re.VERBOSE
>>> re.search(pattern, 'M', re.VERBOSE)

// Sintassi {x}, indica un numero preciso di x di valori da ricercare, non di più e non di meno. 
// \d cerca un qualsiasi numero da 0 a 9, \D cerca qualsiasi digitazione non numerica, \D+ cerca una o più caratteri digitati, \D* significa 0 o più caratteri digitati
// Per costruire il pattern si usa re.compile(), per cercare si utilizza search().groups() perchè si cerca il numero in search() andando nei groups() creati da re.compile()
// Ognuno dei seguenti search() funziona con il re.compile() che lo precede, se si volesse provare un search() con un compile precedente al proprio assegnato non funzionerà
>>> phonePattern = re.compile(r'^(\d{3})\D*(\d{3})\D*(\d{4})\D*(\d*)$')
>>> phonePattern.search('80055512121234').groups()
>>> phonePattern.search('800.555.1212 x1234').groups()
>>> phonePattern.search('800-555-1212').groups()
>>> phonePattern = re.compile(r'^\D*(\d{3})\D*(\d{3})\D*(\d{4})\D*(\d*)$')
>>> phonePattern.search('(800)5551212 ext. 1234').groups()
>>> phonePattern = re.compile(r'(\d{3})\D*(\d{3})\D*(\d{4})\D*(\d*)$')
>>> phonePattern.search('work 1-(800) 555.1212 #1234').groups()

// Verbose regular expressions del re.compile() finale, in questo modo anche riaccendendo al sorgente mesi dopo se ne facilita la comprensione generale
>>> phonePattern = re.compile(r'''
                # don't match beginning of string, number can start anywhere
    (\d{3})     # area code is 3 digits (e.g. '800')
    \D*         # optional separator is any number of non-digits
    (\d{3})     # trunk is 3 digits (e.g. '555')
    \D*         # optional separator
    (\d{4})     # rest of number is 4 digits (e.g. '1212')
    \D*         # optional separator
    (\d*)       # extension is optional and can be any number of digits
    $           # end of string
    ''', re.VERBOSE)

Riassunto:
^ matches the beginning of a string.
$ matches the end of a string.
\b matches a word boundary.
\d matches any numeric digit.
\D matches any non-numeric character.
x? matches an optional x character (in other words, it matches an x zero or one times).
x* matches x zero or more times.
x+ matches x one or more times.
x{n,m} matches an x character at least n times, but not more than m times.
(a|b|c) matches exactly one of a, b or c.
(x) in general is a remembered group. You can get the value of what matched by using the groups() method of the object returned by re.search().

